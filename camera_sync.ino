/*
*** Distribution of synchronization pulse from lidar Ouster to four cameras            ***
*** Output of lidar connect to pin "INPUT_BY_LIDAR", cameras connect to pins "cameras" ***
*/

#include <ros.h>
#include <std_msgs/UInt32MultiArray.h>

#define INPUT_BY_LIDAR 2
#define PULSE_WIDTH 2000 // Pulse width in microseconds
#define NUMBER_OF_CAMERAS 4

class Camera {
public:
    Camera(int pinNumber) {
        pin = pinNumber;
        pinMode(pin, OUTPUT);
    }
    unsigned long local_timer = 0;
    int pin;
    bool isOn = false;

    void isOn_clear() {
        isOn = false;
    }
};

ros::NodeHandle nh;

void Callback(std_msgs::UInt32MultiArray& pulses_time);
ros::Subscriber<std_msgs::UInt32MultiArray> sub("sync_msg", &Callback);

//const int cameras[] = {4, 5, 6, 7}; // pins for cameras
volatile bool pulse = false;
uint32_t offset_time[NUMBER_OF_CAMERAS] = {10000, 40000, 60000, 90000};
//unsigned long main_timer = 0;
unsigned long additional_timer = 0;
int counter = 0;
Camera first_cam(4);
Camera second_cam(5);
Camera third_cam(6);
Camera forth_cam(7);

void setup() {
    pinMode(INPUT_BY_LIDAR, INPUT);
    nh.initNode();
    nh.subscribe(sub);
    attachInterrupt(0, InputPulse, RISING);
}

void InputPulse() {
    pulse = true;
}

void Callback(std_msgs::UInt32MultiArray& pulses_time) {
    for (int i = 0; i < NUMBER_OF_CAMERAS; i++) {
        offset_time[i] = pulses_time.data[i];
    }
}

void PulseGenerator(Camera& camera) {
    additional_timer = micros();
    digitalWrite(camera.pin, HIGH);
    while ((micros() - additional_timer) < PULSE_WIDTH) {
        ; // wait...
    }
    digitalWrite(camera.pin, LOW);
    camera.isOn = true;
}

void Distribution() {
    first_cam.local_timer  = micros();
    second_cam.local_timer = micros();
    third_cam.local_timer  = micros();
    forth_cam.local_timer  = micros();
    
    while (counter < NUMBER_OF_CAMERAS) {
        // pulse for first camera
        if (micros() - first_cam.local_timer >= offset_time[0] && !first_cam.isOn) {
            PulseGenerator(first_cam);
            counter++;
        }

        // pulse for second camera
        if (micros() - second_cam.local_timer >= offset_time[1] && !second_cam.isOn) {
            PulseGenerator(second_cam);
            counter++;
        }

        // pulse for third camera
        if (micros() - third_cam.local_timer >= offset_time[2] && !third_cam.isOn) {
            PulseGenerator(third_cam);
            counter++;
        }

        // pulse for forth camera
        if (micros() - forth_cam.local_timer >= offset_time[3] && !forth_cam.isOn) {
            PulseGenerator(forth_cam);
            counter++;
        }
    }
    counter = 0;
    first_cam.isOn_clear();
    second_cam.isOn_clear();
    third_cam.isOn_clear();
    forth_cam.isOn_clear();
}

void loop() {
    if (pulse) {
        pulse = false;
        Distribution();
    }
    nh.spinOnce();
}
